'use strict';

/**
 * Ride.js controller
 *
 * @description: A set of functions called "actions" for managing `Ride`.
 */

module.exports = {

  /**
   * Retrieve ride records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.ride.search(ctx.query);
    } else {
      return strapi.services.ride.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a ride record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.ride.fetch(ctx.params);
  },

  /**
   * Count ride records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.ride.count(ctx.query);
  },

  /**
   * Create a/an ride record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.ride.add(ctx.request.body);
  },

  /**
   * Update a/an ride record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.ride.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an ride record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.ride.remove(ctx.params);
  },

  endRide: async (ctx)=>{
    var body = ctx.request.body;
    var ride = await Ride.findOne({'_id':mongoose.Types.ObjectId(body.ride_id)})
    var distance = strapi.services.user.calculateDistance(body.current_location,ride.start_point)
    var fare = distance*10.0
    var result = await Ride.findOneAndUpdate({'_id':mongoose.Types.ObjectId(body.ride_id)},{'actual_fare':fare,'status':'finished'})
    ctx.send(fare);
  }
};

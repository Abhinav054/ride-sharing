'use strict';

/**
 * User.js controller
 *
 * @description: A set of functions called "actions" for managing `User`.
 */
const mongoose = require('mongoose')

module.exports = {

  /**
   * Retrieve user records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.user.search(ctx.query);
    } else {
      return strapi.services.user.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a user record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.user.fetch(ctx.params);
  },

  /**
   * Count user records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.user.count(ctx.query);
  },

  /**
   * Create a/an user record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.user.add(ctx.request.body);
  },

  /**
   * Update a/an user record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.user.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an user record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.user.remove(ctx.params);
  },

  searchCabs: async (ctx) =>{
    var body = ctx.request.body;
    // var user = await User.findOne({'_id':id})
    var drivers = await Driver.find();
    var reponse = []
    for(var d of drivers){
      var distance = await strapi.services.user.calculateDistance(d.current_location,body.start_coordinate);
      reponse.push(
        {
          "cab_id":d.id,
          "driver_id":d.id,
          "distance":d,
          "eta":(d/30.0)*60
        }
      )
      ctx.send(reponse);
    }
  },

  bookCab: async (ctx)=>{
    var body  = ctx.request.body;
    var driver = await Driver.findOne();
    var distance = await strapi.services.user.calculateDistance(body.start_coordinate,driver.current_location);
    var trip_distance = await strapi.services.user.calculateDistance(body.start_coordinate,body.end_coordinate);
    var estimated_fare = trip_distance*10
    var eta = (distance/30.0)*60
    var ride = await Ride.create({"user":body.user_id,"start_point":body.start_coordinate,"end_point":body.end_coordinate,"driver":driver._id,"estimated_fare":estimated_fare,"status":"ongoing"});
    ctx.send(
      {
        "driver_id":driver.id,
        "cab_id":driver.id,
        "eta":eta,
        "estimated_fare":estimated_fare,
        "ride_id":ride.id
      }
    ) 
  }
};
